
demon.count = 0

local spawn_radius = minetest.settings:get("demon_imp_spawn_radius")
local despawn_radius = minetest.settings:get("demon_imp_despawn_radius")
local spawn_thickness = minetest.settings:get("demon_imp_spawn_thickness")
local spawn_height = minetest.settings:get("demon_imp_spawn_height")
local despawn_chance = minetest.settings:get("demon_imp_despawn_chance")
local attack_radius = minetest.settings:get("demon_imp_attack_radius")


local def = {
	initial_properties = {
		physical = true,
		collide_with_objects = true,
		collisionbox = {-0.4, 0, -0.4, 0.4, 1.2, 0.4},
		selectionbox = {-0.4, 0, -0.4, 0.4, 1.2, 0.4},
		pointable = true,
		visual = "mesh",
		mesh = "imp.b3d",
		visual_size = {x = 5, y = 5, z = 5},
		textures = {"demon_imp.png"},
		use_texture_alpha = false,
		is_visible = true,
		makes_footstep_sound = false,
		automatic_rotate = 0,
		stepheight = 2,  -- ability to climb up nodes
        automatic_face_movement_dir = 0.0,
		automatic_face_movement_max_rotation_per_sec = 180,
		backface_culling = true,
		glow = 0,
		nametag = "",
		infotext = "",
		static_save = true,
		damage_texture_modifier = "^[brighten",
		shaded = true,
		show_on_minimap = false,
	},

	on_activate = function(self, staticdata, dtime_s)
		self:_stop()
		self.object:set_acceleration({x=0,y=-10,z =0})		-- set the entity gravity, you only need to do it 1 time
		self.step_count = 0
		demon.count = demon.count + 1
	end,

	on_deactivate = function(self, staticdata, dtime_s)
		demon.count = demon.count - 1
	end,

	on_death = function(self, killer)
		minetest.sound_play("demon_die", {
			object = self.object,
			gain = 0.5,
			loop = false,
		})
		demon.guts_at(self.object:get_pos())
	end,

	on_punch = function(self, puncher, time_from_last_punch, tool_capabilities, dir, damage)

		minetest.sound_play("demon_hit", {
			object = self.object,
			gain = 0.5,
			loop = false,
		})
		
		local knockback_v = dir:multiply(2)
		local mypos = self.object:get_pos()

		demon.blood_at(mypos)

		local knockback_to = mypos:add(knockback_v):round()
		knockback_to.y = mypos.y
		local node = minetest.get_node(knockback_to)

		if node.name == "air" then
			self.object:move_to(knockback_to, true)
		end

	end,

	on_step = function(self, dtime, moveresult)
		self.step_count = self.step_count + 1
		if self.step_count < 20 then return end

		self.step_count = 0
		
		if math.random(7) == 1 then
			minetest.sound_play("demon_imp", {
				object = self.object,
				loop = false,
			})
		end

		local mypos = self.object:get_pos()
		local target = self._target

		if target then
			local target_pos = target:get_pos()
			if not target_pos then
				self:_stop()
				return 
			end

			local new_path = minetest.find_path(mypos, target_pos, 20, 1.5, 3)
			local path = new_path or self._path
			self._path = path

			-- default without path
			local delta = target_pos:subtract(mypos)
			-- if there is a path - use it instead
			if path then
				while #path > 0 do
					local point_at = table.remove(path,1)
					delta = point_at:subtract(mypos)
					if delta:length() > 1.6 then break end
				end
			end
					
			local v =  delta:normalize():multiply(3)
			local old_v = self.object:get_velocity()

			-- check for floating
			local node = minetest.get_node(mypos)
			local node_def = minetest.registered_nodes[node.name]
			if node_def.liquidtype == "none" then
				self.object:set_acceleration({x=0,y=-10,z =0})
				v.y = old_v.y
			else
				self.object:set_acceleration({x=0,y=0, z=0})
				v.y = v.y + 1
			end

			self.object:set_velocity(v)

			local distance = target_pos:subtract(mypos):length()

			if distance < 1.8 then
				minetest.sound_play("demon_imp_bite", {
					object = self.object,
					gain = 0.5,
					loop = false,
				})
				target:punch(
					self.object,
					1.0, 
					{full_punch_interval = 2.0, damage_groups = {fleshy = 3} },
					nil
				)
			elseif distance > 20 then
				self:_stop()
			end
		else
			local search_radius = math.random(attack_radius - 1) + 1
			local targets = minetest.get_objects_inside_radius(mypos, search_radius)
			for _,o in ipairs(targets) do
				if minetest.is_player(o) then
					self:_run(o)
					return
				end
			end

			if self._walkpath then
				local point_at = self._walkpath[1]
				local delta = point_at:subtract(mypos)

				while delta:length() < 1 do
					point_at = table.remove(self._walkpath,1)
					if not point_at then break end
					delta = point_at:subtract(mypos)
				end

				if point_at == nil or #(self._walkpath) == 0 then
					self:_stop()
					return
				end

				local v =  delta:normalize():multiply(0.6)
				local old_v = self.object:get_velocity()
				v.y = old_v.y
				self.object:set_velocity(v)
				return
			end

			if math.random(301 - 3*despawn_chance) == 1 then -- despawn
				local targets = minetest.get_objects_inside_radius(mypos, despawn_radius)
				for _,o in ipairs(targets) do
					-- don't despawn if player is within despawn radius
					if minetest.is_player(o) then
						return
					end
				end

				self.object:remove()
				return
			end

			if math.random(12) == 1 then -- wander
				local randomplace = demon.spawn_point_in_cylinder(mypos, 3, 15, 10)
				if not randomplace then return end

				local path = minetest.find_path(mypos, randomplace, 20, 1.5, 3)
				if path then self:_walk(path) end

				return
			end

		end
	end,

	_run = function(self, run_to) 
		self.object:set_animation({x=0, y=40}, 40)
		self._target = run_to
	end,

	_walk = function(self, path) 
		self.object:set_animation({x=41, y=64}, 14)
		self._walkpath = path
	end,

	_stop = function(self)
		self.object:set_velocity(vector.zero())
		self.object:set_animation({x=18, y=22}, 0.5)
		self._walkpath = nil
		self._target = nil
	end,
}


-- cylinder spawn pattern
demon.spawn_fns.imp = function(dtime, pos, total_players)
	local max = total_players * 15
	if demon.count > max then return end

	local spawn_at = demon.spawn_point_in_cylinder(pos, spawn_radius, spawn_thickness, spawn_height)
	if not spawn_at then return end

	local light = minetest.get_node_light(spawn_at)
	if light > 7 then return end

	local check_above = spawn_at:copy()
	check_above.y = check_above.y + 1
	local check_node = minetest.get_node(check_above)
	if check_node.name ~= "air" then return end

	minetest.add_entity(spawn_at, "demon:imp")
end

minetest.register_entity("demon:imp", def)
