[imp]
#Radius around player that imp will not spawn within this is 2d in the x and z plane (not y)
demon_imp_spawn_radius (No spawn Radius) int 20

#Radius beyond which imps might despawn.
demon_imp_despawn_radius (Despawn radius) int 40

#How agressivly despawning occurs (100 is fastest despawning) 
demon_imp_despawn_chance (Despawn percent chance) int 50

#imps spawn within a hollow cylinder shape, this is the thickness of the cylinder, the bigger the value, the wider the spawning area
demon_imp_spawn_thickness (Spawn thickness) int 40

#This is the distance above or below the player in which spawns occur
demon_imp_spawn_height (Spawn height) int 20

#This is the distance from the player that the imp will chase player. This is not exact, there is also a random component.
demon_imp_attack_radius (Attack distance) int 20
