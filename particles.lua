demon.blood_at = function(pos)
	
	p = pos:copy()
	p.y = p.y + 1

	minetest.add_particlespawner({
		amount = 5,
		time = 0.25,
		minpos = p,
		maxpos = p,
		minvel = {x = -4, y = -10, z = -4},
		maxvel = {x = 4, y = 4, z = 4},
		minacc = {x = 0, y = -10, z = 0},
		maxacc = {x = 0, y = -10, z = 0},
		minexptime = 0.1,
		maxexptime = 2,
		minsize = 0.4,
		maxsize = 1,
		texpool = {"demon_blood.png", "demon_guts.png"},
	})
end

demon.guts_at = function(pos)
	
	p = pos:copy()
	p.y = p.y + 1

	minetest.add_particlespawner({
		amount = 5,
		time = 0.25,
		minpos = p,
		maxpos = p,
		minvel = {x = -4, y = -10, z = -4},
		maxvel = {x = 4, y = 4, z = 4},
		minacc = {x = 0, y = -10, z = 0},
		maxacc = {x = 0, y = -10, z = 0},
		minexptime = 0.1,
		maxexptime = 2,
		minsize = 2,
		maxsize = 5,
		texpool = {"demon_blood.png", "demon_guts.png"},
	})
end
