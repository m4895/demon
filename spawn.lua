
demon.spawn_fns = {}

minetest.register_globalstep(function (dtime)
	if math.random(10) ~= 1 then return end

	local players = minetest.get_connected_players()
	for _, player in ipairs(players) do
		local player_vel = player:get_velocity()
		local player_pos = player:get_velocity()

		-- offset player spawn location to adjust for player velocity
		local origin = player:get_velocity():multiply(2):add(player:get_pos())

		for k, spawn_fn in pairs(demon.spawn_fns) do
			spawn_fn(dtime, origin, #players)
		end
	end
end)

demon.spawn_point_in_cylinder = function(pos, radius, thickness, height)
	if not pos then return end
	-- some sane defaults in case not supplied
	radius = radius or 30
	thickness = thickness or 40
	height = height or 20

	-- random vector in horizontal plane
	local rand_v = vector.new(math.random(100) - 50,0,math.random(100) - 50):normalize()
	local random_len =  radius + math.random(thickness)

	-- decrease chance of closer spawn (to get more even distribution)
	if math.random(random_len) < radius then return end

	local displacement = rand_v:multiply(random_len)

	-- use a 3x3 column to scan for suitable spawn site
	local start_pos = pos:add(displacement)
	start_pos.y = start_pos.y - height
	start_pos.x = start_pos.x - 1
	start_pos.z = start_pos.z - 1
	local end_pos = start_pos:copy()
	end_pos.y = end_pos.y + height
	end_pos.x = start_pos.x + 1
	end_pos.z = start_pos.z + 1

	local nodes = minetest.find_nodes_in_area_under_air(start_pos, end_pos,{"group:cracky", "group:crumbly"}, false)
	if not nodes or #nodes < 1 then return end

	local spawn_at = nodes[math.random(#nodes)]
	spawn_at.y = spawn_at.y + 1

	return spawn_at
end


